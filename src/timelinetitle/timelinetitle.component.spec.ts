import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimelinetitleComponent } from './timelinetitle.component';

describe('TimelinetitleComponent', () => {
  let component: TimelinetitleComponent;
  let fixture: ComponentFixture<TimelinetitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimelinetitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelinetitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
