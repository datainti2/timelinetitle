import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimelinetitleComponent } from './src/timelinetitle/timelinetitle.component';
import { SlimScrollModule } from 'ng2-slimscroll';

export * from './src/timelinetitle/timelinetitle.component';


@NgModule({
  imports: [
    CommonModule,
    SlimScrollModule
  ],
  declarations: [
    TimelinetitleComponent,
  ],
  exports: [
    TimelinetitleComponent,

  ]
})
export class TimelinetitleModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: TimelinetitleModule,
      providers: []
    };
  }
}
